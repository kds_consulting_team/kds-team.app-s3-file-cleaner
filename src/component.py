'''
Template Component main class.

'''

import csv
import logging
import os
import sys
from datetime import datetime

import boto3
import pytz
import tzlocal
from kbc.env_handler import KBCEnvHandler

# configuration variables

# aws params
KEY_AWS_PARAMS = 'aws_parameters'
KEY_AWS_API_KEY_ID = 'api_key_id'
KEY_AWS_API_KEY_SECRET = '#api_key_secret'
KEY_AWS_REGION = 'aws_region'
KEY_AWS_S3_BUCKET = 's3_bucket'

KEY_FILE_PATTERNS = 'file_patterns'

KEY_UNTIL_DATE = 'until_date'

AUDIT_LOG_FILE_NAME = 'audit_log.csv'
ERROR_LOG_FILE_NAME = 'error_log.csv'
AUDIT_FILE_COLS = ['Key', 'LastModified', 'ETag', 'Size', 'StorageClass', 'deleted_timestamp', 'run_id', 'pattern']
ERROR_FILE_COLS = ['Key', 'VersionId', 'Code', 'Message', 'error_timestamp', 'run_id']
# #### Keep for debug
KEY_DEBUG = 'debug'

MANDATORY_PARS = [KEY_UNTIL_DATE, KEY_AWS_PARAMS, KEY_FILE_PATTERNS]
MANDATORY_IMAGE_PARS = []

APP_VERSION = '0.0.1'


class Component(KBCEnvHandler):

    def __init__(self, debug=False):
        KBCEnvHandler.__init__(self, MANDATORY_PARS)
        # override debug from config
        if self.cfg_params.get(KEY_DEBUG):
            debug = True
        if debug:
            logging.getLogger().setLevel(logging.DEBUG)
        logging.info('Running version %s', APP_VERSION)
        logging.info('Loading configuration...')

        try:
            self.validate_config(MANDATORY_PARS)
            self.validate_parameters(self.cfg_params[KEY_AWS_PARAMS],
                                     [KEY_AWS_API_KEY_ID, KEY_AWS_API_KEY_SECRET, KEY_AWS_REGION, KEY_AWS_S3_BUCKET],
                                     KEY_AWS_PARAMS)
        except ValueError as e:
            logging.exception(e)
            exit(1)
        self.client = boto3.client('s3',
                                   region_name=self.cfg_params[KEY_AWS_PARAMS][KEY_AWS_REGION],
                                   aws_access_key_id=self.cfg_params[KEY_AWS_PARAMS][KEY_AWS_API_KEY_ID],
                                   aws_secret_access_key=self.cfg_params[KEY_AWS_PARAMS][KEY_AWS_API_KEY_SECRET])

    def run(self):
        '''
        Main execution code
        '''
        params = self.cfg_params  # noqa
        run_id = os.getenv('KBC_RUNID', '')

        start_date, end_date = self.get_date_period_converted(params[KEY_UNTIL_DATE], 'today')
        start_date = pytz.timezone(tzlocal.get_localzone().zone).localize(start_date)
        audit_log_path = os.path.join(self.tables_out_path, AUDIT_LOG_FILE_NAME)
        error_log_path = os.path.join(self.tables_out_path, ERROR_LOG_FILE_NAME)
        with open(audit_log_path, mode='w+', encoding='utf-8') as audit_f, \
                open(error_log_path, mode='w+', encoding='utf-8') as errors_f:
            audit_writer = csv.DictWriter(audit_f, fieldnames=AUDIT_FILE_COLS, lineterminator='\n')
            audit_writer.writeheader()

            error_writer = csv.DictWriter(errors_f, fieldnames=ERROR_FILE_COLS, lineterminator='\n')
            error_writer.writeheader()

            del_files_cnt = 0
            bucket = params[KEY_AWS_PARAMS][KEY_AWS_S3_BUCKET]
            for p in params[KEY_FILE_PATTERNS]:
                logging.info(f'Removing files with prefix: {p}')

                for to_del_page in self._get_s3_objects(bucket, p, until=start_date):
                    logging.debug(to_del_page)
                    del_files_cnt += len(to_del_page.keys())
                    # delete records
                    deleted, errors = self.delete_s3_objects(bucket, list(to_del_page.keys()))

                    # audit
                    audit_records = self.build_audit_records(to_del_page, run_id, p)
                    logging.debug(audit_records)
                    if audit_records:
                        audit_writer.writerows(audit_records)
                    if errors:
                        error_writer.writerows(self.build_error_records(errors, run_id))

        self.configuration.write_table_manifest(audit_log_path, primary_key=['Key'], incremental=True)
        self.configuration.write_table_manifest(error_log_path, primary_key=['Key', 'error_timestamp', 'run_id'],
                                                incremental=True)

        if del_files_cnt:
            logging.info(f"Job finished, {del_files_cnt} files deleted.")
        else:
            logging.warning("Job finished, but 0 files deleted.")

    def delete_s3_objects(self, bucket, keys):
        if not keys:
            return [], []

        objects = [{'Key': k} for k in keys]
        errors = []
        resp = self.client.delete_objects(
            Bucket=bucket,
            Delete={
                'Objects': objects,
                'Quiet': False
            }
        )
        if resp['ResponseMetadata']['HTTPStatusCode'] > 299:
            raise RuntimeError(f'Failed to delete keys! {resp}')

        if len(resp['Deleted']) != len(keys):
            logging.warning(f'Some keys could not be deleted! {resp.get("Errors", [])}')
            errors = resp.get("Errors", [])

        logging.debug(resp)
        return resp.get('Deleted', []), errors

    def _get_s3_objects(self, bucket, prefix, until=None):
        if prefix.endswith('*'):
            is_wildcard = True
            prefix = prefix[:-1]
        else:
            is_wildcard = False

        paginator = self.client.get_paginator('list_objects_v2')
        params = dict(Bucket=bucket,
                      Prefix=prefix,
                      PaginationConfig={
                          'MaxItems': 100000,
                          'PageSize': 1000
                      })
        has_more = True
        counter = 0
        while has_more:
            pages = paginator.paginate(**params)
            keys = {}
            for page in pages:
                for obj in page.get('Contents', []):
                    key = obj['Key']
                    # skip folders
                    if key.endswith('/'):
                        continue

                    if until and obj['LastModified'] > until:
                        continue

                    if is_wildcard and key.startswith(prefix):
                        counter += 1
                        keys[obj['Key']] = obj
                    elif key == prefix:
                        counter += 1
                        keys[obj['Key']] = obj

                if page.get('NextContinuationToken'):
                    has_more = True
                    params['PaginationConfig']['StartingToken'] = page['NextContinuationToken']
                else:
                    has_more = False

                yield keys

    def build_audit_records(self, deleted_files, run_id, pattern):
        audit_records = []
        if not deleted_files:
            return
        for f in deleted_files:
            record = deleted_files[f]
            record['LastModified'] = record['LastModified'].isoformat()
            record['deleted_timestamp'] = datetime.utcnow().isoformat()
            record['run_id'] = run_id
            record['pattern'] = pattern

            audit_records.append(record)
        return audit_records

    def build_error_records(self, errors, run_id):
        error_records = []
        if not errors:
            return
        for f in errors:
            record = f
            record['run_id'] = run_id
            record['error_timestamp'] = datetime.utcnow().isoformat()

            error_records.append(record)
        return error_records


"""
        Main entrypoint
"""
if __name__ == "__main__":
    if len(sys.argv) > 1:
        debug_arg = sys.argv[1]
    else:
        debug_arg = False
    try:
        comp = Component(debug_arg)
        comp.run()
    except Exception as exc:
        logging.exception(exc)
        exit(1)
