Clear S3  files based on prefix and last updated date.

### Functionality notes

- `LastUpdated` timestamp is used to get the file age.
- `*` wildcard may be used, **all files in all sub folders** matching the pattern will be deleted.
- Only files are deleted, folders remain intact.
- The app will continue on failure when some of the files can't be deleted. List of files, that failed to be removed is provided in `error_log` result table
- Each deleted file is logged in the `audit_log` result table