# S3 file cleaner

Remove all files older than some date and matching pattern.

# Functionality notes

- `LastUpdated` timestamp is used to get the file age.
- `*` wildcard may be used, **all files in all sub folders** matching the pattern will be deleted.
- Only files are deleted, folders remain intact.
- The app will continue on failure when some of the files can't be deleted. List of files, that failed to be removed is provided in `error_log` result table
- Each deleted file is logged in the `audit_log` result table

# Configuration

- **Remove data before [including]** - Remove files older than this date (inclousive). Date in YYYY-MM-DD format or dateparser string i.e. 5 days ago, 1 month ago, yesterday, etc. If left empty, all found files are deleted.
- **File Patterns** - List of file patterns to remove. May include `*` wildcards. e.g. `myfolder/*`. 
**NOTE**: All files in all subfolders matching the pattern will be removed.
 
## Development
 
This example contains runnable container with simple unittest. For local testing it is useful to include `data` folder in the root
and use docker-compose commands to run the container or execute tests. 

If required, change local data folder (the `CUSTOM_FOLDER` placeholder) path to your custom path:
```yaml
    volumes:
      - ./:/code
      - ./CUSTOM_FOLDER:/data
```

Clone this repository, init the workspace and run the component with following command:

```
git clone https://bitbucket.org:kds_consulting_team/kbc-python-template.git my-new-component
cd my-new-component
docker-compose build
docker-compose run --rm dev
```

Run the test suite and lint check using this command:

```
docker-compose run --rm test
```

# Integration

For information about deployment and integration with KBC, please refer to the [deployment section of developers documentation](https://developers.keboola.com/extend/component/deployment/) 